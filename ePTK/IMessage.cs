﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ePTK
{
    /// <summary>
    /// user group types
    /// </summary>
    public enum UserGroup
    {
        Admin = 0,
        Office = 1
    }

    /// <summary>
    /// Interface that represents a message
    /// </summary>
    public interface IMessage
    {
        /// <summary>
        /// Group of users, the message should be sent
        /// </summary>
        UserGroup GetUserGroup();

        /// <summary>
        /// Subject text
        /// </summary>
        String GetSubject();

        /// <summary>
        /// Message text
        /// </summary>
        String GetMessage();
    }
}
