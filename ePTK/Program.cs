﻿using System;
using System.Threading;

namespace ePTK
{
    static class Program
    {
        static ManualResetEvent _quitEvent = new ManualResetEvent(false);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            Console.CancelKeyPress += (sender, eArgs) =>
            {
                _quitEvent.Set();
                eArgs.Cancel = true;
            };

            TenderChecker checker = new TenderChecker();
            checker.Start();

            _quitEvent.WaitOne();
        }
    }
}
