﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePTK.Logging
{
    public sealed class Logger
    {
        private static volatile Logger _Instance;
        private static readonly object _InstanceLockObject = new Object();

        private List<String> _Logs;
        private readonly object _LogLockObject = new Object();

        private Logger()
        {
            this._Logs = new List<String>();
        }

        /// <summary>
        /// Returns singleton logger
        /// </summary>
        public static Logger Instance
        {
            get
            {
                if (_Instance == null)
                {
                    lock(_InstanceLockObject)
                    {
                        if (_Instance == null)
                        {
                            _Instance = new Logger();
                        }
                    }
                }

                return _Instance;
            }
        }

        /// <summary>
        /// Write error level message
        /// </summary>
        public void Error(String message)
        {
            this.Write("ERROR", message);
        }

        /// <summary>
        /// Write Info level message
        /// </summary>
        public void Info(String message)
        {
            this.Write("INFO", message);
        }

        /// <summary>
        /// Returns logs and emptys the logger
        /// </summary>
        /// <returns></returns>
        public String Prune()
        {
            StringBuilder builder = new StringBuilder();

            lock (_LogLockObject)
            {
                foreach (String log in _Logs)
                {
                    builder.AppendLine(log);
                }
                _Logs.Clear();
            }

            return builder.ToString();
        }

        /// <summary>
        /// Writes message to log
        /// </summary>
        private void Write(String level, String message)
        {
            String logMessage = String.Format("{0} [{1}] {2}", DateTime.Now.ToString(), level, message);

            Console.WriteLine(logMessage);

            lock (_LogLockObject)
            {
                _Logs.Add(logMessage);
            }
        }
    }
}
