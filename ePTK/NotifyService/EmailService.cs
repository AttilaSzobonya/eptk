﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ePTK.NotifyService
{
    public class EmailService : INotifyRecipient
    {
        public EmailService()
        {
            this.TargetAddresses = new List<EmailRecipient>();
        }

        public List<EmailRecipient> TargetAddresses { get; set; }

        public DnsEndPoint SmtpServerEndPoint { get; set; }

        public NetworkCredential Credentials { get; set; }

        public string SenderAddress { get; set; }

        public void Notify(IMessage message)
        {
            foreach (var recipient in this.TargetAddresses.Where(x => (int)x.UserGroup <= (int)message.GetUserGroup()))
            {
                using (var client = new SmtpClient(this.SmtpServerEndPoint.Host, this.SmtpServerEndPoint.Port)
                 {
                     Credentials = this.Credentials,
                     EnableSsl = true
                 })
                {
                    client.Send(this.SenderAddress, recipient.Address, message.GetSubject(), message.GetMessage());
                }
            }
        }
    }
}
