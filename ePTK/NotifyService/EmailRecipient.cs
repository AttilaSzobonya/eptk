﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePTK.NotifyService
{
   public class EmailRecipient
    {
        public string Address { get; set; }
        public UserGroup UserGroup { get; set; }
    }
}
