﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePTK.Messages
{
   public class KeepAliveMessage :IMessage
    {
        public UserGroup GetUserGroup()
        {
            return UserGroup.Admin;
        }

        public string GetSubject()
        {
            return "I'm alive";
        }

        public string GetMessage()
        {
            return "Peet told me to tell you about it.";
        }
    }
}
