﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using ePTK.Commands;
using ePTK.Logging;

namespace ePTK
{
    /// <summary>
    /// Represents the state of the tender
    /// </summary>
    enum TenderState
    {
        Checking = 0,
        Open = 1,
        Closed = 2
    }

    /// <summary>
    /// Represents a tender checker
    /// </summary>
    class TenderChecker
    {
        /// <summary>
        /// Current state of the tender
        /// </summary>
        private Boolean TenderWasOpen { get; set; }

        /// <summary>
        /// Timer for checking state
        /// </summary>
        private Timer CheckTimer { get; set; }


        /// <summary>
        /// Creates a Tenderchecker
        /// </summary>
        public TenderChecker()
        {
            this.TenderWasOpen = false;

            this.CheckTimer = new Timer();
            this.CheckTimer.Interval = 15000;
            this.CheckTimer.AutoReset = true;
            this.CheckTimer.Elapsed += CheckTimer_Elapsed;
        }

        /// <summary>
        /// Check tender state
        /// </summary>
        private void CheckTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.DoCheck();
        }


        /// <summary>
        /// Start polling tender site
        /// </summary>
        public void Start()
        {
            if (!this.CheckTimer.Enabled)
            {
                this.CheckTimer.Enabled = true;
                this.DoCheck();
            }
        }

        /// <summary>
        /// Starts checking tender state
        /// </summary>
        private void DoCheck()
        {
            this.OnTenderStateChanged(TenderState.Checking);

            if (this.IsTenderOpen())
            {
                this.OnTenderStateChanged(TenderState.Open);
            }
            else
            {
                this.OnTenderStateChanged(TenderState.Closed);
            }
        }


        /// <summary>
        /// Check if tender is open
        /// </summary>
        private Boolean IsTenderOpen()
        {
            try
            {
                WebClientWithCookies webClient = new WebClientWithCookies();

                var login = new Login(webClient);
                CommandResult<String> loginExecution = login.Execute();
                if (loginExecution.State != CommandState.Finished)
                {
                    throw new Exception("Login failed!");
                }

                var checkTenderStatus = new CheckTenderStatus(webClient);
                CommandResult<Boolean> checkTenderExecution = checkTenderStatus.Execute();
                if (checkTenderExecution.State != CommandState.Finished)
                {
                    throw new Exception("Failed to check tender state!");
                }

                var logout = new Logout(webClient);
                CommandResult<String> logoutExecution = logout.Execute();
                if (logoutExecution.State != CommandState.Finished)
                {
                    throw new Exception("Logout failed!");
                }

                // return if the tender is open
                return checkTenderExecution.Result;
            }
            catch (Exception ex)
            {
                // logging
                Logger.Instance.Error(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Fires a tender state changed event
        /// </summary>
        private void OnTenderStateChanged(TenderState state)
        {
            switch (state)
            {
                case TenderState.Checking:
                    Logger.Instance.Info("Checking ...");
                    break;

                case TenderState.Open:
                    if (!this.TenderWasOpen)
                    {
                        this.TenderWasOpen = true;

                        Logger.Instance.Info("Tender state changed!");

                        // send an email
                        Messaging.GetNotifier().Notify(Messaging.GetIntoOffice());
                    }

                    Logger.Instance.Info("Tender is Open!");
                    break;

                case TenderState.Closed:
                    if (this.TenderWasOpen)
                    {
                        this.TenderWasOpen = false;

                        Logger.Instance.Info("Tender state changed!");

                        // send an email
                        Messaging.GetNotifier().Notify(Messaging.GetOutOfOffice());
                    }
                    Logger.Instance.Info("Tender is Closed!");
                    break;
            }
        }
    }
}
