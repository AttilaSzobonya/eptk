﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ePTK.Commands
{
    class Login : WebCommand<String>
    {
        private static String LoginUrl = "https://eptk.fair.gov.hu/?p=UqQD_B-mRBqvptMbDcWYxN07U%253D";

        /// <summary>
        /// Creates a login command
        /// </summary>
        public Login(WebClientWithCookies webClient)
            : base(webClient)
        { }

        /// <summary>
        /// Executes the command
        /// </summary>
        public override CommandResult<String> Execute()
        {
            try
            {
                // 1. get login token
                String token = this.GetLoginToken();

                // 2. post login form
                String postResponse = this.PostLoginForm(token);

                // 3. verify the login
                Boolean isLoggedIn = this.VerifyLogin(postResponse);

                return new CommandResult<String> { Result = "Logged in successfuly!", State = CommandState.Finished };
            }
            catch (Exception)
            {
                return new CommandResult<String> { Result = "Login failed!", State = CommandState.Failed };
            }
        }

        /// <summary>
        /// Get login token
        /// </summary>
        private String GetLoginToken()
        {
            this.WebClient.Method = WebRequestMethod.GET;

            String loginPage = this.WebClient.DownloadString(LoginUrl);

            Regex tokenRegex = new Regex("<input type=\"hidden\" value=\"(\\w+)\" name=\"token\">", RegexOptions.IgnoreCase);
            Match tokenMatch = tokenRegex.Match(loginPage);
            if (tokenMatch.Success)
            {
                return tokenMatch.Groups[1].Value;
            }

            return "";
        }

        /// <summary>
        /// Post login form
        /// </summary>
        private String PostLoginForm(String token)
        {
            this.WebClient.Method = WebRequestMethod.POST;

            String body = String.Format("token={0}&form-post-name=id-login-form&FELH%5BEMAIL%5D=mfviki%40gmail.com&FELH_SSO%5BPASSWORD_HASH%5D=20AlbaSansz02&yt0=&createRedirect=", token);
            return this.WebClient.UploadString(LoginUrl, body);
        }

        /// <summary>
        /// Verify that the login was successful
        /// </summary>
        private Boolean VerifyLogin(String postResponse)
        {
            return postResponse.Contains("Automatikus kijelentkezésig:");
        }
    }
}
