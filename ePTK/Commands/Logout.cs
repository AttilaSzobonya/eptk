﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePTK.Commands
{
    class Logout : WebCommand<String>
    {
        private static String LogoutUrl = "https://eptk.fair.gov.hu/?p=iy7goWYZnDrXpw%253D%253D";

        /// <summary>
        /// Creates a CheckTenderStatus command
        /// </summary>
        public Logout(WebClientWithCookies webClient)
            : base(webClient)
        { }

        /// <summary>
        /// Executes the command
        /// </summary>
        public override CommandResult<String> Execute()
        {
            try
            {
                this.LogoutOnPage();

                return new CommandResult<String> { Result = "Logged out successfuly!", State = CommandState.Finished };
            }
            catch (Exception)
            {
                return new CommandResult<String> { Result = "Logout failed!", State = CommandState.Finished };
            }
        }

        private Boolean LogoutOnPage()
        {
            this.WebClient.Method = WebRequestMethod.GET;

            String logoutPage = this.WebClient.DownloadString(LogoutUrl);
            Boolean isSignedIn = logoutPage.Contains("Automatikus kijelentkezésig:");

            return !isSignedIn;
        }
    }
}
