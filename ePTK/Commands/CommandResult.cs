﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePTK.Commands
{
    /// <summary>
    /// Represents a command state
    /// </summary>
    public enum CommandState
    {
        Finished = 0,
        Failed = 1
    }

    /// <summary>
    /// Represents a command result
    /// </summary>
    struct CommandResult<T>
    {
        public T Result { get; set; }

        public CommandState State { get; set; }
    }
}
