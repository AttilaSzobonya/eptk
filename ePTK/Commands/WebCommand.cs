﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePTK.Commands
{
    /// <summary>
    /// Represents a command
    /// </summary>
    abstract class WebCommand<T>
    {
        /// <summary>
        /// WebClient to use for web requests
        /// </summary>
        protected WebClientWithCookies WebClient { get; set; }

        /// <summary>
        /// Creates a command
        /// </summary>
        public WebCommand(WebClientWithCookies webClient)
        {
            this.WebClient = webClient;
        }

        /// <summary>
        /// To execute the command
        /// </summary>
        /// <returns>True on success, false on failure</returns>
        public abstract CommandResult<T> Execute();
    }
}
