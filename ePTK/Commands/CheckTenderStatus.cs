﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePTK.Commands
{
    class CheckTenderStatus : WebCommand<Boolean>
    {
        private static String TenderUrl = "https://eptk.fair.gov.hu/?p=iBQYrb1MCuEU2mT9jnzFxBqnyfwpk4ju_B-UQW";

        /// <summary>
        /// Creates a CheckTenderStatus command
        /// </summary>
        public CheckTenderStatus(WebClientWithCookies webClient)
            : base(webClient)
        { }

        /// <summary>
        /// Executes the command
        /// </summary>
        public override CommandResult<Boolean> Execute()
        {
            try
            {
                Boolean isTenderOpen = this.CheckTender();

                return new CommandResult<Boolean> { Result = isTenderOpen, State = CommandState.Finished };
            }
            catch (Exception)
            {
                return new CommandResult<Boolean> { Result = false, State = CommandState.Failed };
            }
        }

        /// <summary>
        /// Checks if tender is closed
        /// </summary>
        private Boolean CheckTender()
        {
            this.WebClient.Method = WebRequestMethod.GET;

            String tenderPage = this.WebClient.DownloadString(TenderUrl);
            if (!this.IsLoggedIn(tenderPage))
            {
                throw new UnauthorizedAccessException();
            }

            return this.IsTenderOpen(tenderPage);
        }

        /// <summary>
        /// Returns if we are logged in
        /// </summary>
        private Boolean IsLoggedIn(String content)
        {
            return content.Contains("Automatikus kijelentkezésig:");
        }

        /// <summary>
        /// Returns if the selected tender is open
        /// </summary>
        private Boolean IsTenderOpen(String content)
        {
            return !content.Contains("időponttól felfüggesztett állapotban van, ezért nincs lehetőség a támogatási kérelem benyújtására.");
        }
    }
}
