﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePTK.Messages
{
    public class GeneralMessage : IMessage
    {
        internal GeneralMessage(UserGroup userGroup, string subject, string message)
        {
            _userGroup = userGroup;
            _subject = subject;
            _message = message;
        }

        private UserGroup _userGroup;
        private string _subject;
        private string _message;

        public UserGroup GetUserGroup()
        {
            return _userGroup;
        }

        public string GetSubject()
        {
            return _subject;
        }

        public string GetMessage()
        {
            return _message;
        }
    }
}
