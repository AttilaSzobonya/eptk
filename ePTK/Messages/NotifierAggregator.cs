﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePTK.Messages
{
    public class NotifierAggregator : INotifyRecipient
    {
        internal NotifierAggregator()
        {
        }

        private List<INotifyRecipient> _notifiers = new List<INotifyRecipient>();

        public void RegisterNotifier(INotifyRecipient notifier)
        {
            _notifiers.Add(notifier);
        }

        public void Notify(IMessage message)
        {
            foreach (var notifier in _notifiers)
            {
                notifier.Notify(message);
            }
        }
    }
}
