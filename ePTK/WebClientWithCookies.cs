﻿using System;
using System.Net;

namespace ePTK
{
    enum WebRequestMethod
    {
        POST = 0,
        GET = 1
    }

    class WebClientWithCookies : WebClient
    {
        /// <summary>
        /// Cookie container
        /// </summary>
        public CookieContainer CookieContainer { get; set; }

        /// <summary>
        /// Method
        /// </summary>
        public WebRequestMethod Method;

        /// <summary>
        /// Creates a WebClient with cookies
        /// </summary>
        public WebClientWithCookies()
            : base()
        {
            this.CookieContainer = new CookieContainer();
            this.Encoding = System.Text.Encoding.UTF8;
        }

        /// <summary>
        /// Overrides GetWebRequest with cookie and browser specific informations
        /// </summary>
        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);

            HttpWebRequest webRequest = request as HttpWebRequest;
            if (webRequest != null)
            {
                webRequest.CookieContainer = this.CookieContainer;
                webRequest.ServicePoint.Expect100Continue = false;
                webRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36";
                webRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
                webRequest.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-US,en;q=0.8,hu;q=0.6");
                webRequest.Host = "eptk.fair.gov.hu";
                webRequest.KeepAlive = true;
                webRequest.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

                if (Method == WebRequestMethod.POST)
                {
                    webRequest.ContentType = "application/x-www-form-urlencoded";
                }

            }

            return request;
        }
    }
}
