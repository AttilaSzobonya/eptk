﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePTK
{
    public interface INotifyRecipient
    {
        void Notify(IMessage message);
    }
}
